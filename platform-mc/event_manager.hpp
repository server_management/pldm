#pragma once

#include "libpldm/platform.h"
#include "libpldm/requester/pldm.h"

#include "common/types.hpp"
#include "numeric_sensor.hpp"
#include "pldmd/dbus_impl_requester.hpp"
#include "requester/handler.hpp"
#include "terminus.hpp"

namespace pldm
{
namespace platform_mc
{

/**
 * @brief EventManager
 *
 * This class handles PLDM Platform Event Message from terminus and then invoke
 * related handle functions to process different type of received messages.
 */
class EventManager
{
  public:
    EventManager() = delete;
    EventManager(const EventManager&) = delete;
    EventManager(EventManager&&) = delete;
    EventManager& operator=(const EventManager&) = delete;
    EventManager& operator=(EventManager&&) = delete;
    ~EventManager() = default;

    explicit EventManager(
        sdeventplus::Event& event,
        pldm::requester::Handler<pldm::requester::Request>& handler,
        pldm::dbus_api::Requester& requester,
        std::map<mctp_eid_t, std::shared_ptr<Terminus>>& termini) :
        event(event),
        handler(handler), requester(requester), termini(termini){};

    void pollForEvent(uint8_t tid);

  private:
    requester::Coroutine pollForEventTask(mctp_eid_t eid);
    requester::Coroutine pollForPlatformEventMessage(
        mctp_eid_t eid, uint8_t transferOpFlag, uint32_t dataTransferHndl,
        uint16_t eventIdToAck, uint8_t& tid, uint16_t& eventId,
        uint32_t& nextDataTransferHndl, uint8_t& transferFlag,
        uint8_t& eventClass, std::vector<uint8_t>& eventData,
        uint32_t& checksum);
    void processEventMessage(uint8_t tid, uint8_t eventClass, uint16_t eventID,
                             const std::vector<uint8_t>& eventMessage);

    sdeventplus::Event& event;
    pldm::requester::Handler<pldm::requester::Request>& handler;
    pldm::dbus_api::Requester& requester;

    std::map<mctp_eid_t, std::shared_ptr<Terminus>>& termini;
};
} // namespace platform_mc
} // namespace pldm
