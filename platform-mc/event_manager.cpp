#include "event_manager.hpp"
#include <stdio.h>
#include <phosphor-logging/lg2.hpp>

namespace pldm
{
namespace platform_mc
{

namespace fs = std::filesystem;

void EventManager::pollForEvent(uint8_t tid)
{
    for (auto terminus : termini)
    {
        if (terminus.second->tid() == tid)
        {
            pollForEventTask(terminus.first);
        }
    }
}

requester::Coroutine EventManager::pollForEventTask(mctp_eid_t eid)
{
    std::printf("pollForEventTask(%d)\n", eid);

    std::vector<uint8_t> eventMessage{};
    uint8_t tid = 0;
    uint8_t transferOpFlag = PLDM_EVENT_GET_FIRST_PART;
    uint32_t dataTransferHndl = 0;
    uint16_t eventIdToAck = 0;
    uint16_t eventId = 0;
    uint32_t nextDataTransferHndl = 0;
    uint8_t transferFlag = 0;
    uint8_t eventClass = 0;
    std::vector<uint8_t> eventData{};
    uint32_t checksum = 0;

    do
    {
        eventData.clear();
        auto rc = co_await pollForPlatformEventMessage(
            eid, transferOpFlag, dataTransferHndl, eventIdToAck, tid, eventId,
            nextDataTransferHndl, transferFlag, eventClass, eventData,
            checksum);
        if (rc)
        {
            co_return rc;
        }

        if (eventData.size() > 0)
        {
            eventMessage.insert(eventMessage.end(), eventData.begin(),
                                eventData.end());
        }

        std::printf("transferFlag=%d eventId=0x%04x\n", transferFlag, eventId);
        if (eventId == 0x0)
        {
            break;
        }
        else if (eventId == 0xffff)
        {
            transferOpFlag = PLDM_EVENT_GET_FIRST_PART;
            dataTransferHndl = 0;
            eventIdToAck = 0;
        }
        else
        {
            switch (transferFlag)
            {
                case PLDM_EVENT_START:
                case PLDM_EVENT_MIDDLE:
                    transferOpFlag = PLDM_EVENT_GET_NEXT_PART;
                    dataTransferHndl = nextDataTransferHndl;
                    eventIdToAck = 0xffff;
                    break;

                case PLDM_EVENT_START_AND_END:
                case PLDM_EVENT_END:
                    processEventMessage(tid, eventClass, eventId, eventMessage);
                    eventMessage.clear();

                    transferOpFlag = PLDM_EVENT_ACKNOWLEDGEMENT_ONLY;
                    dataTransferHndl = 0x0;
                    eventIdToAck = eventId;
                    break;

                default:
                    co_return PLDM_ERROR;
            }
        }
    } while (true);

    co_return PLDM_SUCCESS;
}

requester::Coroutine EventManager::pollForPlatformEventMessage(
    mctp_eid_t eid, uint8_t transferOpFlag, uint32_t dataTransferHndl,
    uint16_t eventIdToAck, uint8_t& tid, uint16_t& eventId,
    uint32_t& nextDataTransferHndl, uint8_t& transferFlag, uint8_t& eventClass,
    std::vector<uint8_t>& eventData, uint32_t& checksum)
{
    std::printf("pollForPlatformEventMessage(%d) transferOpFlag=%d\n", eid,
                transferOpFlag);
    auto instanceId = requester.getInstanceId(eid);
    std::vector<uint8_t> requestMsg(
        sizeof(pldm_msg_hdr) + PLDM_POLL_FOR_PLATFORM_EVENT_MESSAGE_REQ_BYTES);
    auto request = reinterpret_cast<pldm_msg*>(requestMsg.data());
    auto rc = encode_poll_for_platform_event_message_req(
        instanceId, PLDM_PLATFORM_EVENT_MESSAGE_FORMAT_VERSION, transferOpFlag,
        dataTransferHndl, eventIdToAck, request,
        PLDM_POLL_FOR_PLATFORM_EVENT_MESSAGE_REQ_BYTES);
    if (rc)
    {
        requester.markFree(eid, instanceId);
        co_return rc;
    }

    std::vector<uint8_t> responseMsg{};
    rc = co_await requester::sendRecvPldmMsg(handler, eid, requestMsg,
                                             responseMsg);
    if (rc)
    {
        co_return rc;
    }

    uint8_t completionCode = 0;
    auto response = reinterpret_cast<pldm_msg*>(responseMsg.data());
    auto length = responseMsg.size() - sizeof(struct pldm_msg_hdr);
    uint8_t* data = nullptr;
    uint32_t dataSize = 0;
    rc = decode_poll_for_platform_event_message_resp(
        response, length, &completionCode, &tid, &eventId,
        &nextDataTransferHndl, &transferFlag, &eventClass, &dataSize, &data,
        &checksum);
    if (rc)
    {
        co_return rc;
    }
    eventData.insert(eventData.end(), data, data + dataSize);
    co_return completionCode;
}

void EventManager::processEventMessage(uint8_t tid, uint8_t eventClass,
                                       uint16_t eventId,
                                       const std::vector<uint8_t>& eventMessage)
{
    if (eventClass == PLDM_CPER_EVENT)
    {
        // save eventData to file and a log to syslog for record.
        std::string eventName = "PLDM_EVENT";
        std::string messageId = "PldmEvent.0.1.CPER";
        std::string messageArgs = "";
        std::string messageData =
            "event" + std::to_string(tid) + "_" + std::to_string(eventId);
        std::string eventDirPath = "/var/log/pldm.d";
        if (!fs::is_directory(eventDirPath))
        {
            fs::create_directories(eventDirPath);
        }

        std::ofstream output_file(eventDirPath + "/" + messageData);
        std::ostream_iterator<uint8_t> output_iterator(output_file);
        std::copy(eventMessage.begin(), eventMessage.end(), output_iterator);
        output_file.close();

        lg2::info("CPER Event", "EVENT", eventName, "PLDM_EVENT_MESSAGE_ID",
                  messageId, "PLDM_EVENT_MESSAGE_DATA", messageData,
                  "PLDM_EVENT_MESSAGE_ARGS", messageArgs);
    }
    else
    {
        std::printf("log event eventClass=%d\n", eventClass);
        // todo: handle other event classes
    }
}

} // namespace platform_mc
} // namespace pldm
