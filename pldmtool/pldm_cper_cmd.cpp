#include <string>
#include <ctime>
#include <iostream>
#include <filesystem>
#include <vector>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
extern "C" 
{
#include "../libcper/cper-parse.h"
#include "../libcper/generator/cper-generate.h"
#include <json.h>
}
#include "pldm_cper_cmd.hpp"

namespace pldmtool 
{

//Reads PLDM CPER logs from /var/log/pldm.d/ and outputs them to console.
void cper::readPlatformCperLogs()
{
    //Get a list of files within /var/log/pldm.d/.
    std::string path = "/var/log/pldm.d/";

    //If the directory doesn't exist, error out.
    if (!std::filesystem::is_directory(path))
    {
        std::printf("No log directory found at /var/log/pldm.d/. Exiting.\n");
        return;
    }

    //Iterate over entries, read.
    for (const auto & entry : std::filesystem::directory_iterator(path)) {

        //Open a read stream to that file.
        std::ifstream cperLogFile(entry.path(), std::ios::in | std::ios::binary);
        
        //Read out the format type, length.
        std::vector<uint8_t> data((std::istreambuf_iterator<char>(cperLogFile)), std::istreambuf_iterator<char>());
        uint8_t formatType = data[1];
        uint16_t cperDataLength = *((uint16_t*)(&data[2]));

        //Open a stream to the CPER event data.
        void* cperDataPtr = (void*)(&data[4]);
        FILE* stream = fmemopen(cperDataPtr, cperDataLength, "r");

        //Determine type, convert accordingly.
        json_object* cper_json;
        if (formatType == 0x00) { //CPER full record header.
            cper_json = cper_to_ir(stream);
        }
        else if (formatType == 0x01) {
            cper_json = cper_single_section_to_ir(stream);
        }
        else {
            std::printf("message was of unknown CPER format type. ignoring.");
            continue;
        }

        std::cout << entry.path() << "\n";
        auto json_txt = json_object_to_json_string_ext(cper_json, JSON_C_TO_STRING_PRETTY);
        std::printf("%s\n", json_txt);
        std::cout << "\n\n";

        fclose(stream);
    }
}

//Injects a CPER log into the system for reading by pldmtool/bmcweb.
void cper::injectCperLog()
{
    std::string logDirectory = "/var/log/pldm.d/";

    //If the log directory doesn't exist, create it.
    if (!std::filesystem::is_directory(logDirectory))
        std::filesystem::create_directories(logDirectory);

    //Get an output file handle to random log file.
    srand((unsigned)time(NULL));
    std::string logFileName = "event_" + std::to_string(rand() % 100) + "_" + std::to_string(rand() % 10);
    std::string logFilePath = logDirectory + logFileName;
    FILE* logFile = fopen(logFilePath.c_str(), "w");

    //Write header, then generate record to file.
    uint8_t logHeader[] = { 0x0, 0x1, 0x0, 0x0 };
    fwrite(logHeader, sizeof(logHeader), 1, logFile);
    generate_single_section_record(const_cast<char*>("arm"), logFile);

    //Seek and write log length.
    uint16_t length = (uint16_t)(ftell(logFile) - sizeof(logHeader));
    fseek(logFile, 2, SEEK_SET);
    fwrite(&length, sizeof(length), 1, logFile);
    fclose(logFile);

    //Get timestamp string.
    time_t now;
    time(&now);
    char timeString[sizeof "0000-00-00T00:00:00Z"];
    strftime(timeString, sizeof(timeString), "%FT%TZ", gmtime(&now));

    //Add record to top level log file.
    std::string logLine = std::string(timeString) + " PldmEvent.0.1.CPER," + logFileName + ",\n";
    std::ofstream outfile;
    outfile.open("/var/log/pldm", std::ios_base::app); //Append mode.
    outfile << logLine;
    outfile.close(); 
}

}